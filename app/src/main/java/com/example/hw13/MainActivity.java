package com.example.hw13;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("Hello", "OnCreateActivity");
        Intent intent = new Intent(MainActivity.this, MyService.class);
        intent.putExtra("TIME EXTRA", 5);

        Button start = findViewById(R.id.startBtn);
        Button stop = findViewById(R.id.stopBtn);
        Button sendNtfc = findViewById(R.id.sendNotification);
        Button sendIntentExtra = findViewById(R.id.sendIntentExtra);

        ProgressBar progressBar = findViewById(R.id.progressBar);

        MyService myService = new MyService();

        sendIntentExtra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MyIntentService.class);
                intent.putExtra("TIME_EXTRA", 3).putExtra("TEXT_EXTRA", "Call 1");
                intent.putExtra("TIME_EXTRA", 2).putExtra("TEXT_EXTRA", "Call 2");
                intent.putExtra("TIME_EXTRA", 1).putExtra("TEXT_EXTRA", "Call 3");

                startService(intent);
            }
        });

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                startService(intent);
            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.GONE);
                stopService(intent);
            }
        });

        sendNtfc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(MainActivity.this, NotificationService.class);
                startService(intent1);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("Hello", "onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("Hello", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Hello", "onDestroy");
    }
}