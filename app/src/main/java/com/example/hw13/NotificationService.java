package com.example.hw13;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import java.util.concurrent.TimeUnit;

public class NotificationService extends Service {

    private final String NOTIFICATION_CHANNEL_ID = "NOTIFICATION_CHANNEL_ID";
    private NotificationManager  notificationManager;
    private final int NOTIFY_ID = 100;
    private final int REQUEST_CODE = 1000;

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        createChannel();
        //sendNotification();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Hello", "notificationService Send");
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    TimeUnit.SECONDS.sleep(10);
                    sendNotification();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return  null;
    }

    private void createChannel(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Service", NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription("Channel for Service");
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    private void sendNotification(){
        Intent intent = new Intent(NotificationService.this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(NotificationService.this, REQUEST_CODE, intent, PendingIntent.FLAG_CANCEL_CURRENT   );
        NotificationCompat.Builder builder = new NotificationCompat.Builder(NotificationService.this, NOTIFICATION_CHANNEL_ID);
        builder.setSmallIcon(R.drawable.ic_launcher_foreground);
        builder.setContentTitle("Service Notification");
        builder.setContentText("Notification from service");
        builder.setAutoCancel(true);
        builder.setContentIntent(pendingIntent);
        Notification notification = builder.build();
        notificationManager.notify(NOTIFY_ID, notification);
    }

}