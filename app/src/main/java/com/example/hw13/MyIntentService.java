package com.example.hw13;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.JobIntentService;

import java.sql.Time;
import java.util.concurrent.TimeUnit;

public class MyIntentService extends JobIntentService {

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("Hello", "onCreateIntentService");
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        int time = intent.getIntExtra("TIME_EXTRA", 0);
        String text = intent.getStringExtra("TEXT_EXTRA");
        Log.d("Hello", "Start " +  text);
        try{
            TimeUnit.SECONDS.sleep(time);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        Log.d("Hello", "End " + text);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("Hello", "onDestroyIntentService");
    }
}
