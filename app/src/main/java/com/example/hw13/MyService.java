package com.example.hw13;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.TimeUnit;

public class MyService extends Service {
    public MyService() {
    }

    @Override
    public void onCreate() {
        Log.d("Hello", "OnCreateService");
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        Log.d("Hello", "OnDestroyService");
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Hello", "OnStartCommandService");
        if(flags == START_FLAG_REDELIVERY){
            Log.d("Hello", "START_FLAG_REDELIVERY");
        }
        else if (flags ==START_FLAG_RETRY){
            Log.d("Hello", "START_FLAG_REDELIVERY");
        }
        int time = intent.getIntExtra("TIME_EXTRA", 0);
        //doSomeTask(time);
        Log.d("Hello", "Service #" + startId);
        //return super.onStartCommand(intent, flags, startId);
        return  Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("Hello", "OnBindService");
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private  void doSomeTask(int time){
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=0; i<time; i++)
                {
                    Log.d("Hello", "For " + i);
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                stopSelf();
            }
        }).start();
    }
}